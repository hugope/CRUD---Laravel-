<!DOCTYPE html>
<html lang="en" class="loading">
<!-- section BEGIN : Head-->
@include("layaut.head")

<!-- END : Head-->

<!-- section BEGIN : Body-->
<body data-col="2-columns" class=" 2-columns ">
<!-- ////////////////////////////////////////////////////////////////////////////-->
<div class="wrapper">


    <!-- section main menu-->
    @include("layaut.leftMenu")
    <!-- section Navbar (Header) Starts-->
    @include("layaut.topMenu")
    <!-- Navbar (Header) Ends-->

    <div class="main-panel">
        <!-- section BEGIN : Main Content-->
        <div class="main-content">
            <div class="content-wrapper"><!-- Zero configuration table -->
                @yield('content')
            </div>
        </div>
        <!-- END : End Main Content-->

        <!-- section BEGIN : Footer-->
        <footer class="footer footer-static footer-light">
            <p class="clearfix text-muted text-sm-center px-2"><span>Copyright  &copy; 2019 <a href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" id="pixinventLink" target="_blank" class="text-bold-800 primary darken-2">PIXINVENT </a>, All rights reserved. </span></p>
        </footer>
        <!-- End : Footer-->

    </div>
</div>
<!-- ////////////////////////////////////////////////////////////////////////////-->


<!-- BEGIN Botons right-->
@include("layaut.botonsRight")
<!-- section BEGIN VENDOR JS-->
@include("layaut.scripts")
<!-- END PAGE LEVEL JS-->
</body>
<!-- END : Body-->
</html>
