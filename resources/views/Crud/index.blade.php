@extends('layaut.master')
@section('title', $titleMsg)

@section("css")
@endsection

@section('content')
    @if(!isset($request->form))
        <div class="row" style="display: {{ $titleVisible ? 'none' : 'block'}}">
            <div class="col-sm-12">
                <div class="content-header">
                    <a href="/{{ base64_encode($table) }}" style="color: black">
                        {!! $titleMsg !!}
                    </a>
                </div>
            </div>
        </div>
    @endif
    {!! $vista !!}
    @if(isset($dataTableView))
        @include("Crud._tblUniversal", $dataTableView)
    @endif
@stop

@section('js')
@endsection
