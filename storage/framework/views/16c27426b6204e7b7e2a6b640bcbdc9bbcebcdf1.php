<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title" id="horz-layout-basic"><?php echo e($subTitle); ?></h4>
                <p class="mb-0">Por favor llenar los campos para hacer una <?php echo e($accion); ?> a la tabla <?php echo e($table); ?>.</p>
            </div>
            <div class="card-content">
                <div class="px-3">
                    <form class="form form-horizontal" method="post" action="/insert_in_table" id="frmInsert">
                        <input type="hidden" id="_token" name="_token" value="<?php echo e(csrf_token()); ?>">
                        <input type="hidden" id="Id" name="Id" value="<?php echo e($id); ?>">
                        <input type="hidden" id="table" name="table" value="<?php echo e($table); ?>">
                        <input type="hidden" id="redirect" name="redirect" value="<?php echo e($urlAnt); ?>">
                        <div class="form-body">
                            <h4 class="form-section"><i class="ft-file-text"></i> Requerimientos</h4>
                            <?php if(count($inputs)>0): ?>
                                <?php $__currentLoopData = $inputs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ki => $vi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="form-group row">
                                        <label class="col-md-3 label-control" for="<?php echo e($ki); ?>"><?php echo e(str_replace("Id","",$ki)); ?>: </label>
                                        <div class="col-md-9">
                                            <?php echo $vi; ?>

                                        </div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                        </div>
                        <div class="form-actions">
                            <?php if(!$request->form): ?>
                                <a type="button" class="btn btn-raised btn-warning mr-1" href="<?php echo e($urlAnt ? $urlAnt : url()->current()); ?>">
                                    <i class="ft-arrow-left"></i> Regresar
                                </a>
                            <?php endif; ?>
                            <button type="submit" class="btn btn-raised btn-primary" id="btnInserta">
                                <i class="ft-save"></i> Guardar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php /**PATH C:\xampp\htdocs\CrudLaravel\resources\views/Crud/_frmCreate.blade.php ENDPATH**/ ?>