<section id="shopping-cart">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"><?php echo e($subTitle); ?></h4>
                    <p class="card-text">A continuación un listado de l@s <?php echo e($subTitle); ?> cread@s, si deseas crear
                        más
                        favor dar click
                        <a class="btn btn-flat btn-danger"
                           href="/<?php echo e(base64_encode($table)."?".base64_encode("crear=1")); ?>">aquí</a>
                    </p>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <?php if($dataTable && $dataTable->count() > 0): ?>
                            <table class="table  datatablePointer">
                                <thead>
                                <tr class="headings darken-4 bg-dark">
                                    <th class="column-title th-acciones" style="color: white;width: 110px">ACCIONES</th>
                                    <?php $__currentLoopData = $dataTable->first(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($k!== "Id" && $k!== "FechaCreacion" && $k!== "FechaModificacion" && $k!== "UsuarioCreacion" && $k!== "UsuarioModificacion"): ?>
                                            <th class="column-title" style="color: white;">
                                                <?php echo e($dataFK->where("constraint_column_name",$k)->first() ? str_replace("ID", "",strtoupper ($k)) : strtoupper ($k)); ?>

                                            </th>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tr>
                                </thead>

                                <tbody>
                                <?php $__currentLoopData = $dataTable; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $krs => $vrs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr class="">
                                        <td class="text-center">
                                            <a href="/<?php echo e(base64_encode($table)."?".base64_encode("editar=$vrs->Id")); ?>"
                                               class="btn btn-flat btn-info overOrange aActiveModal tooltip-per"
                                               title="Modificar item">
                                                <i class="ft-edit-2"></i>
                                            </a>
                                            <a href="/<?php echo e(base64_encode($table)."?".base64_encode("ver=$vrs->Id")); ?>"
                                               class="btn btn-flat btn-success overOrange aActiveModal tooltip-per"
                                               title="Ver item">
                                                <i class="ft-eye"></i>
                                            </a>
                                            <a onclick="deleteData('<?php echo e($vrs->Id); ?>')"
                                               href="javascript:void(0);"
                                               class="btn btn-flat btn-danger overOrange tooltip-per delete">
                                                <i class="ft-trash"></i>
                                            </a>
                                        </td>
                                        <?php ($arrayFK = $dataFK->pluck("dataTable","constraint_column_name")->toArray()); ?>
                                        <?php $__currentLoopData = $dataTable->first(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ki => $vi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($ki!== "Id" && isset($arrayFK[$ki][$vrs->$ki])): ?>
                                                <td class=" "><?php echo e(trim($arrayFK[$ki][$vrs->$ki])); ?></td>
                                            <?php elseif($ki!== "Id" && $ki!== "FechaCreacion" && $ki!== "FechaModificacion" && $ki!== "UsuarioCreacion" && $ki!== "UsuarioModificacion"): ?>
                                                <td class=" "><?php echo e($vrs->$ki); ?></td>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        <?php else: ?>
                            <div class="alert alert-warning alert-dismissible fade show"
                                 role="alert" id="tableUsers">
                                <div class="text"><i class="fa fa-database"></i> La consulta no gener&oacute;
                                    resultados.
                                </div>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true" class="la la-close"></span>
                                </button>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php /**PATH C:\xampp\htdocs\CrudLaravel\resources\views/Crud/_tblUniversal.blade.php ENDPATH**/ ?>