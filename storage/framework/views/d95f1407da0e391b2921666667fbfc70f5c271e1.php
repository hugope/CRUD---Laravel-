<!--.main-menu(class="#{menuColor} #{menuOpenType}", class=(menuShadow == true ? 'menu-shadow' : ''))-->
<div data-active-color="white" data-background-color="man-of-steel" data-image="app-assets/img/sidebar-bg/01.jpg" class="app-sidebar">
    <!-- main menu header-->
    <!-- Sidebar Header starts-->
    <div class="sidebar-header">
        <div class="logo clearfix"><a href="/" class="logo-text float-left">
                <div class="logo-img"><img src="app-assets/img/logo.png"/></div><span class="text align-middle"><br> auto crud</span></a><a id="sidebarToggle" href="javascript:;" class="nav-toggle d-none d-sm-none d-md-none d-lg-block"><i data-toggle="expanded" class="toggle-icon ft-toggle-right"></i></a><a id="sidebarClose" href="javascript:;" class="nav-close d-block d-md-block d-lg-none d-xl-none"><i class="ft-x"></i></a></div>
    </div>
    <!-- Sidebar Header Ends-->
    <!-- / main menu header-->
    <!--   main menu content-->
    <div class="sidebar-content">
        <div class="nav-container">
            <ul id="main-menu-navigation" data-menu="menu-navigation" data-scroll-to-active="true" class="navigation navigation-main">

                <li class=" nav-item"><a href="/"><i class="ft-calendar"></i><span data-i18n="" class="menu-title">Calendar</span></a>
                </li>
                <li class="has-sub nav-item"><a href="#"><i class="ft-aperture"></i><span data-i18n="" class="menu-title">UI Kit</span></a>
                    <ul class="menu-content">
                        <li><a href="/" class="menu-item">Text Utilities</a>
                        </li>
                        <li class="has-sub"><a href="#" class="menu-item">Icons</a>
                            <ul class="menu-content">
                                <li><a href="/" class="menu-item">Font Awesome Icon</a>
                                </li>
                                <li class="active"><a href="/" class="menu-item">Feather Icon</a>
                                </li>
                                <li><a href="/" class="menu-item">Simple Line Icon</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- main menu content-->
    <div class="sidebar-background"></div>
    <!-- main menu footer-->
    <!-- include includes/menu-footer-->
    <!-- main menu footer-->
</div>
<!-- / main menu-->
<?php /**PATH C:\xampp\htdocs\CrudLaravel\resources\views/layaut/leftMenu.blade.php ENDPATH**/ ?>