<?php $__env->startSection('title', $titleMsg); ?>

<?php $__env->startSection("css"); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <?php if(!isset($request->form)): ?>
        <div class="row" style="display: <?php echo e($titleVisible ? 'none' : 'block'); ?>">
            <div class="col-sm-12">
                <div class="content-header">
                    <a href="/<?php echo e(base64_encode($table)); ?>" style="color: black">
                        <?php echo $titleMsg; ?>

                    </a>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php echo $vista; ?>

    <?php if(isset($dataTableView)): ?>
        <?php echo $__env->make("Crud._tblUniversal", $dataTableView, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layaut.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\CrudLaravel\resources\views/Crud/index.blade.php ENDPATH**/ ?>