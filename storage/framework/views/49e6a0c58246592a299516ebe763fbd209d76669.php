<!DOCTYPE html>
<html lang="en" class="loading">
<!-- section BEGIN : Head-->
<?php echo $__env->make("layaut.head", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<!-- END : Head-->

<!-- section BEGIN : Body-->
<body data-col="2-columns" class=" 2-columns ">
<!-- ////////////////////////////////////////////////////////////////////////////-->
<div class="wrapper">


    <!-- section main menu-->
    <?php echo $__env->make("layaut.leftMenu", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <!-- section Navbar (Header) Starts-->
    <?php echo $__env->make("layaut.topMenu", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <!-- Navbar (Header) Ends-->

    <div class="main-panel">
        <!-- section BEGIN : Main Content-->
        <div class="main-content">
            <div class="content-wrapper"><!-- Zero configuration table -->
                <?php echo $__env->yieldContent('content'); ?>
            </div>
        </div>
        <!-- END : End Main Content-->

        <!-- section BEGIN : Footer-->
        <footer class="footer footer-static footer-light">
            <p class="clearfix text-muted text-sm-center px-2"><span>Copyright  &copy; 2019 <a href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" id="pixinventLink" target="_blank" class="text-bold-800 primary darken-2">PIXINVENT </a>, All rights reserved. </span></p>
        </footer>
        <!-- End : Footer-->

    </div>
</div>
<!-- ////////////////////////////////////////////////////////////////////////////-->


<!-- BEGIN Botons right-->
<?php echo $__env->make("layaut.botonsRight", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<!-- section BEGIN VENDOR JS-->
<?php echo $__env->make("layaut.scripts", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<!-- END PAGE LEVEL JS-->
</body>
<!-- END : Body-->
</html>
<?php /**PATH C:\xampp\htdocs\CrudLaravel\resources\views/layaut/master.blade.php ENDPATH**/ ?>