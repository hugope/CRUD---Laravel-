<select <?php if(count($atributes)>0): ?>
        <?php $__currentLoopData = $atributes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ka => $va): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php echo $ka.'="'.$va.'"'; ?>

        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php else: ?> class="form-control select2_single cmb<?php echo e($tipoSelected); ?>" id="cmb<?php echo e($tipoSelected); ?>"
        name="cmb<?php echo e($tipoSelected); ?><?php echo e($multiple ? "[0]": null); ?>"
    <?php endif; ?>
    <?php echo e($multiple2 ? "multiple": null); ?>

    <?php echo e($requerido ?  " required " : null); ?> >
    <?php if(!$select): ?>
        <?php if($placeholder): ?><option value=""  selected><?php echo e($placeholder); ?></option><?php endif; ?>
    <?php endif; ?>
    <?php if(!$attr): ?>
        <?php $__currentLoopData = $ordenar ? $model->sort() : $model; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kdp => $vdp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if(is_array($select)): ?>
                <option value="<?php echo e($kdp); ?>" <?php echo e(in_array($kdp,$select) ? "selected" : null); ?>><?php echo e($vdp); ?></option>
            <?php else: ?>
                <option value="<?php echo e($kdp); ?>" <?php echo e($select == $kdp ? "selected" : null); ?>><?php echo e($vdp); ?></option>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php else: ?>
        <?php $__currentLoopData = $attr; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kdp => $vdp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if(is_array($select)): ?>
                <option value="<?php echo e($vdp->Id); ?>" <?php echo e(in_array($vdp->Id,$select) ? "selected" : null); ?> data-attr = "<?php echo e($vdp->Atributo); ?>"><?php echo e($vdp->Nombre); ?></option>
            <?php else: ?>
                <option value="<?php echo e($vdp->Id); ?>" <?php echo e($select == $vdp->Id ? "selected" : null); ?> data-attr = "<?php echo e($vdp->Atributo); ?>"><?php echo e($vdp->Nombre); ?></option>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
</select>

<?php if($debug): ?>
    <?php echo e(dd($model,$tipoSelected)); ?>

<?php endif; ?>
<?php /**PATH C:\xampp\htdocs\CrudLaravel\resources\views/Crud/_selected.blade.php ENDPATH**/ ?>