# CRUD - Laravel

Construye **CRUD** con solo una tabla como parámetro utilizando **PHP** como lenguaje de programación y **LARAVEL** como framework.

Para poderlo utilizar, debe de seguir las siguientes reglas:

    1.- Su clave principal debe llamarse Id
    2.- La tabla debe tener los siguientes campos por default:
        a) Id
        b) FechaCreacion
        c) FechaModificacion 
        d) UsuarioCreacion
        e) UsuarioModificacion
    3.- Preferiblemente los campos de nombre deben de llamarse nombre

¡Listo, disfrútelo!
