<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;

class CrudController extends Controller
{

    var $idUsuarioCreacion;
    var $form = 0;
    var $tableUse = "";

    public function __construct()
    {
        try {
            $this->idUsuarioCreacion = getDataUserLogeado() ? getDataUserLogeado()->Id : null;
        } catch (Exception $e) {

        }

    }

    /**
     * Pantalla principal
     * @param $table
     * @param Request $request
     * @return Factory|View
     * @throws Throwable
     */
    public function index($table, Request $request)
    {
        ini_set('memory_limit', '-1');
        set_time_limit(5000);
        $titleVisible = parse_url(str_replace(url('/'), '', url()->previous()));
        $titleVisible = $titleVisible["path"] == "/crear_listar" ? 1 : 0;
        $msg = \session()->get("msg");
        $libs2Load = ["DataTables" => true, "SweetAlert" => true, "Select2" => true];
        $table = base64_decode($table);
        $this->tableUse = $table;
        $titleMsg = $this->descriptionTable($table);
        $dataFK = $this->dataFK($table);
        $request = UrlToData($request);
        $javaScript = "<script>var ver = false, tabla = '$table'</script>";
        if ($request->crear || $request->form) {
            $vista = $this->viewCreate($table, null, $request);
        } elseif ($request->editar) {
            $vista = $this->viewCreate($table, $request->editar, $request);
        } elseif ($request->ver) {
            $javaScript = "<script>var ver = true, tabla = '$table'</script>";
            $vista = $this->viewCreate($table, $request->ver, $request);
        } else {
            $vista = "";
            $dataTableView = $this->viewTable($table, $dataFK);
        }
        return view("Crud.index", get_defined_vars());
    }

    /**
     * Tabla de contenido de las tablas
     * @param $table
     * @param $dataFK
     * @return array|string
     * @throws Throwable
     */
    private function viewTable($table, $dataFK)
    {
        $dataTable = $this->dataTable($table);
        $subTitle = $this->retirePrefijos($table);
        if ($dataFK->count() > 0) {
            $dataFK = $dataFK->each(function ($v, $k) {
                $v->dataTable = $this->dataArray($v->referenced_object);
            });
        }
        return get_defined_vars();
    }

    /**
     * Formulario de insert o update
     * @param $table
     * @param $id
     * @return array|string
     * @throws Throwable
     */
    private function viewCreate($table, $id, $request)
    {
        $this->form = 1;
        $subTitle = $this->retirePrefijos($table);
        $inputs = $this->returInputs($table, $id);
        $accion = "creación";
        $urlAnt = parse_url(str_replace(url('/'), '', url()->previous()));
        $urlAnt = $urlAnt["path"] == "/" ? url()->previous() : 0;
        if ($request->form) {
            $urlAnt = url()->current();
        }
        return view("Crud._frmCreate", get_defined_vars())->render();
    }

    /**
     * Accion que inserta o actualiza los registros
     * @param Request $request
     * @return RedirectResponse|Redirector
     */
    public function insertInTable(Request $request)
    {
        try {
            $keyId = "Id";
            $keyTable = "table";
            $arrayInsert = array_map(function ($v) {
                return trim($v);
            }, $request->input());
            $id = $request->input($keyId);
            $table = $request->input($keyTable);
            unset($arrayInsert[$keyId], $arrayInsert[$keyTable], $arrayInsert["_token"]);
            $mensajeReturn = ["Tipo" => "success", "Descripcion" => "Datos almacenados correctamente"];
            if (!$id) {
                unset($arrayInsert["redirect"]);
                $arrayInsert["UsuarioCreacion"] = $this->idUsuarioCreacion;
                foreach ($arrayInsert as $kai => $vai) {
                    if ($vai == "") {
                        $arrayInsert[$kai] = null;
                    }
                }
                $insertData = DB::table($table)->insert($arrayInsert);
            } else {
                unset($arrayInsert["redirect"]);
                $arrayInsert["UsuarioModificacion"] = $this->idUsuarioCreacion;
                foreach ($arrayInsert as $kai => $vai) {
                    if ($vai == "") {
                        $arrayInsert[$kai] = null;
                    }
                }
                $insertData = DB::table($table)->where($keyId, $id)->update($arrayInsert);
            }
            if (!$insertData) {
                $mensajeReturn = ["Tipo" => "error", "Descripcion" => "Hubo un problema al momento de realizar la
            inserción, por favor pongase en contacto con el administrador del sistema"];
            }
        } catch (Exception $e) {
            if ($e->getCode() == 23000) {
                $mensajeReturn = ["Tipo" => "error", "Descripcion" => "El Registro que intenta ingresar ya existe"];
            } else {
                $mensajeReturn = ["Tipo" => "error", "Descripcion" => "Hubo un problema al momento de realizar la
            inserción, (" . $e->getMessage() . ")"];
            }
        }

        session()->push('msg', $mensajeReturn);
        $urlReturn = $request->input("redirect") ? $request->input("redirect") :
            "/" . base64_encode($table);
        return redirect($urlReturn);
    }

    /**
     * Elimina datos de una tabla
     * @param $table
     * @param $id
     * @return RedirectResponse|Redirector
     */
    public function delete($table, $id)
    {
        try {
            $mensajeReturn = ["Tipo" => "success", "Descripcion" => "Datos eliminados correctamente"];
            if (!DB::table($table)->where("Id", $id)->delete()) {
                $mensajeReturn = ["Tipo" => "error", "Descripcion" => "Hubo un problema al momento de realizar la
            eliminacion, por favor pongase en contacto con el administrador del sistema"];
            }
        } catch (Exception $e) {
            $mensajeReturn = ["Tipo" => "error", "Descripcion" => "Hubo un problema al momento de realizar la
            inserción, (" . $e->getMessage() . ")"];
        }

        session()->push('msg', $mensajeReturn);
        $urlReturn = "/" . base64_encode($table);
        return redirect($urlReturn);
    }

    /**
     * Retorna los inputs que se mostraran en el formulario de insert o update
     * @param $table
     * @param $id
     * @return array
     * @throws Throwable
     */
    public function returInputs($table, $id)
    {
        $objectValue = null;
        if ($id) {
            $objectValue = DB::table($table)->find($id);
        }

        $arrayTypeColumn = config('constants.columns');
        $dataFK = $this->dataFK($table);
        $dataTableColumns = $this->dataTableColumns($table);
        $inputsReturn = [];
        if ($dataTableColumns->count() > 0) {
            foreach ($dataTableColumns as $kdc => $vdc) {
                $columnName = $vdc->column_name;
                $value = $id ? $objectValue->$columnName : null;
                if (isset($dataFK->where("constraint_column_name", $columnName)->first()->referenced_object)) {
                    $tableName = $dataFK->where("constraint_column_name", $columnName)->first()->referenced_object;
                    $collectUniversal = collect([$this->retirePrefijos($tableName) => $this->dataArray($tableName, 1)]);
                    $inputsReturn[$columnName] = SelectedUniversales($collectUniversal,
                        $value,
                        false, [
                            "name" => $columnName,
                            "id" => $columnName,
                            "class" => "form-control select2_single $columnName"
                        ], !$vdc->is_nullable, false, false);
                } else {
                    $type = isset($arrayTypeColumn[$vdc->type]) ? $arrayTypeColumn[$vdc->type] : "text";
                    $atrubutos = [
                        "name" => $columnName,
                        "id" => $columnName,
                        "type" => isset($arrayTypeColumn[$vdc->type]) ? $arrayTypeColumn[$vdc->type] : "text",
                        "class" => "form-control",
                        "maxlength" => $type == "date" ? null : $vdc->max_length
                    ];
                    if ($vdc->is_nullable == "0") {
                        $atrubutos["required"] = "required";
                    }

                    if ($columnName !== "Descripcion") {
                        $inputsReturn[$columnName] = InputUniversales($atrubutos, null, $value);
                    } else {
                        $atrubutos["rows"] = 3;
                        $inputsReturn[$columnName] = TextareaUniversales($atrubutos, null, $value);
                    }

                }
            }
        }
        return $inputsReturn;
    }

    /**
     * Retorna un arreglo con los nombres de las claves foraneas
     * @param $table
     * @return Collection3
     */
    private function dataFK($table)
    {
        /*// Sql Server
        $queryUniversal = "SELECT
           COL_NAME(fc.parent_object_id, fc.parent_column_id) AS constraint_column_name
           ,OBJECT_NAME (f.referenced_object_id) AS referenced_object
        FROM sys.foreign_keys AS f
        INNER JOIN sys.foreign_key_columns AS fc ON f.object_id = fc.constraint_object_id
        WHERE f.parent_object_id = OBJECT_ID('$table')";*/
        // MySQL
        $queryUniversal = "SELECT COLUMN_NAME AS constraint_column_name,
                      referenced_table_name AS referenced_object
                        FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
                        WHERE REFERENCED_TABLE_SCHEMA = 'crud'
                          AND TABLE_NAME = '$table'";
        return collect(DB::select($queryUniversal));
    }

    /**
     * Retorna la descripcion de la tabla
     * @param $table
     * @return mixed
     */
    private function descriptionTable($table)
    {
        /*// sql Server
        $queryUniversal = "SELECT * FROM
            sys.extended_properties WHERE
            major_id = OBJECT_ID('$table')";*/
        // MySQL
        $queryUniversal = "SELECT table_comment as value
                            FROM INFORMATION_SCHEMA.TABLES
                            WHERE table_schema='crud'
                                AND table_name='$table';";
        $exc = DB::select($queryUniversal);
        return isset($exc[0]) ? $exc[0]->value : $table;
    }

    /**
     * retorna los datos de la tabla
     * @param $table
     * @return Collection
     */
    private function dataTable($table)
    {
        return DB::table($table)->orderBy("Id", "DESC")->get();
    }

    /**
     * Retorna los arrays de las claves foraneas para un combobox
     * @param $table
     * @return Collection
     */
    private function dataArray($table, $input = 0)
    {
        $nombre = $this->reglasName($table, $input)["name"];
        $query = $this->reglasName($table, $input)["query"];
        try {
            return DB::table($table)
                ->select("Id", $query)
                ->distinct()
                ->get()
                ->pluck($nombre, "Id");

        } catch (Exception $e) {
            return collect([]);
        }
    }

    /**
     * reenplaxa los prefijos del nombre de una tabla
     * @param $str
     * @return mixed
     */
    private function retirePrefijos($str)
    {
        $str = str_replace("cd_", "", $str);
        return $str;
    }

    /**
     * Para los combobox necesitamos tener un nombre pero a veces los nombres no siempre se llaman "Nombre"
     * para eso cree este funcion que realiza las reglas dependiendo de la tabla la que se le quiere dar
     * mantenimiento
     * @param $table
     * @return string
     */
    private function reglasName($table, $input = 0)
    {
        switch ($table) {
            case "PX_SIS_Persona":
                $varReturn = ["name" => "Nombres", "query" => DB::raw("CONCAT(Nombres, ' ', Apellidos) as Nombres")];
                break;
            default:
                foreach ($this->dataTableColumns($table) as $k => $v) {
                    if (substr($v->column_name, 0, 2) != "Id" && $this->form) {
                        return $varReturn = [
                            "name" => $v->column_name,
                            "query" => $v->column_name
                        ];
                    } elseif (!$this->form) {
                        return $varReturn = [
                            "name" => $v->column_name,
                            "query" => $v->column_name
                        ];
                    }
                }
                break;
        }
        return $varReturn;
    }

    /**
     * Informacion que se mostrara en la tabla
     * @param $table
     * @return Collection
     */
    private function dataTableColumns($table)
    {
        /*// SQL Server
        $queryUniversal = "SELECT
                        AC.[name] AS [column_name],
                        TY.[name] AS type,
                        AC.[max_length],
                        AC.[is_nullable]
                FROM sys.[tables] AS T
                INNER JOIN sys.[all_columns] AC ON T.[object_id] = AC.[object_id]
                 INNER JOIN sys.[types] TY ON AC.[system_type_id] = TY.[system_type_id] AND AC.[user_type_id] = TY.[user_type_id]
                WHERE T.[is_ms_shipped] = 0  and
                T.[name] = '$table'
                and AC.[name]
                not in ('Id',
                'FechaCreacion',
                'FechaModificacion',
                'UsuarioCreacion',
                'UsuarioModificacion')
                ORDER BY T.[name]";*/

        // MySQL
        $queryUniversal = "select column_name, data_type as type,  REGEXP_SUBSTR(column_type,'[0-9]+') max_length, IF(is_nullable='YES', 1, 0) as is_nullable
                            from information_schema.columns
                            where table_schema = 'crud' and table_name = '$table' and column_name not in ('Id',
                'FechaCreacion',
                'FechaModificacion',
                'UsuarioCreacion',
                'UsuarioModificacion')";

        return collect(DB::select($queryUniversal));
    }

    /**
     * Devuelve json de catalogos
     * @param $model
     * @param $id
     * @param $value
     * @return Collection
     */
    public function reloadCat($model, $id, $value)
    {
        $sqlUniversal = "select $id,$value from $model";
        return collect(DB::select($sqlUniversal))
            ->pluck($id, $value)
            ->sortBy($value)
            ->unique();
    }
}
