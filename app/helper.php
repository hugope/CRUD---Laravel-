<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

if(!function_exists('getDataUserLogeado')) {

    function getDataUserLogeado() {
        return Session::get("Accesos.usuario");
    }
}


if(!function_exists('UrlToData')) {

    function UrlToData(Request $data, $debug = false)
    {
        if ($data->all()) {
            $urlData = base64_decode(array_key_first($data->all()));
            $debug ? dd($urlData) : null;
            $base64ToArray = explode("&", $urlData);
            foreach ($base64ToArray as $kba => $vba) {
                if (trim($vba) != "") {
                    $exploudeInt = explode("=", $vba);
                    try {
                        $ob = "$exploudeInt[0]";
                        $data->$ob = utf8_encode($exploudeInt[1]);
                    } catch (\Exception $e) {
                    }
                }
            }
        }
        return $data;
    }
}

if(!function_exists('InputUniversales')) {

    function InputUniversales($atributes, $name = "", $value = "")
    {
        return view("Crud._input", get_defined_vars())->render();
    }
}

if(!function_exists('TextareaUniversales')) {

    function TextareaUniversales($atributes, $name = "", $value = "")
    {
        return view("Crud._textarea", get_defined_vars())->render();
    }
}

if(!function_exists('SelectedUniversales')) {

    function SelectedUniversales($tipoSelected = null, $select, $activos = false,
                                 $atributes = [], $requerido = false, $select2 = false,
                                 $multiple = false, $multiple2 = false, $debug = false, $ordenar = true,
                                 $placeholder = "Seleccione...", $attr = null)
    {
        $model = collect($tipoSelected->first());
        $tipoSelected = key($tipoSelected->toArray());
        return $tipoSelected ? view("Crud._selected", get_defined_vars())->render() : $tipoSelected;
    }
}

?>
